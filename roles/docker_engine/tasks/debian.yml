---
- name: Create keyrings directory
  ansible.builtin.file:
    path: '/etc/apt/keyrings'
    state: 'directory'
    mode: '755'
  become: true
- name: Create keyring
  ansible.builtin.file:
    path: '/etc/apt/keyrings/{{ docker_apt_keyring }}'
    state: 'touch'
    modification_time: 'preserve'
    access_time: 'preserve'
    mode: '644'
  become: true
- name: Install repository keys
  ansible.builtin.apt_key:
    url: '{{ docker_apt_key_url }}'
    id: '{{ docker_apt_key_id }}'
    keyring: '/etc/apt/keyrings/{{ docker_apt_keyring }}'
  become: true
- name: Install repositories
  ansible.builtin.apt_repository:
    filename: 'docker'
    repo: '{{ docker_apt_repo }}'
  become: true
  register: res_docker_repo
- name: Update APT cache  # noqa no-handler
  ansible.builtin.apt:
    update_cache: true
  become: true
  when: res_docker_repo.changed
- name: Install Docker
  ansible.builtin.package:
    name: 'docker-ce'
    state: 'present'
  notify:
    - 'Restart Docker'
  register: res
  until: res is success
  retries: '{{ package_retries }}'
  delay: '{{ package_delay }}'
  become: true
- name: Install module dependencies
  ansible.builtin.package:
    name:
      - 'python3-yaml'
      - 'python3-requests'
    state: 'present'
  register: res
  until: res is success
  retries: '{{ package_retries }}'
  delay: '{{ package_delay }}'
  become: true
- name: Configure Docker
  ansible.builtin.template:
    src: etc/docker/daemon.json.j2
    dest: /etc/docker/daemon.json
    mode: '600'
  notify:
    - 'Restart Docker'
  become: true
- name: Remove deprecated Python 3 virtualenv
  ansible.builtin.file:
    path: '{{ docker_compose_virtualenv }}'
    state: 'absent'
  become: true
- name: Configure Docker networks
  community.general.docker_network:
    api_version: '{{ item.api_version | default(omit) }}'
    appends: '{{ item.appends | default(omit) }}'
    attachable: '{{ item.attachable | default(omit) }}'
    ca_cert: '{{ item.ca_cert | default(omit) }}'
    client_cert: '{{ item.client_cert | default(omit) }}'
    client_key: '{{ item.client_key | default(omit) }}'
    config_from: '{{ item.config_from | default(omit) }}'
    config_only: '{{ item.config_only | default(omit) }}'
    connected: '{{ item.connected | default(omit) }}'
    debug: '{{ item.debug | default(omit) }}'
    docker_host: '{{ item.docker_host | default(omit) }}'
    driver: '{{ item.driver | default(omit) }}'
    driver_options: '{{ item.driver_options | default(omit) }}'
    enable_ipv6: '{{ item.enable_ipv6 | default(omit) }}'
    force: '{{ item.force | default(omit) }}'
    internal: '{{ item.internal | default(omit) }}'
    ipam_config: '{{ item.ipam_config | default(omit) }}'
    ipam_driver: '{{ item.ipam_driver | default(omit) }}'
    ipam_driver_options: '{{ item.ipam_driver_options | default(omit) }}'
    labels: '{{ item.labels | default(omit) }}'
    name: '{{ item.name | default(omit) }}'
    scope: '{{ item.scope | default(omit) }}'
    ssl_version: '{{ item.ssl_version | default(omit) }}'
    state: '{{ item.state | default(omit) }}'
    timeout: '{{ item.timeout | default(omit) }}'
    tls: '{{ item.tls | default(omit) }}'
    tls_hostname: '{{ item.tls_hostname | default(omit) }}'
    use_ssh_client: '{{ item.use_ssh_client | default(omit) }}'
    validate_certs: '{{ item.validate_certs | default(omit) }}'
  become: true
  with_items: '{{ docker_networks | default([]) }}'
- name: Pull Docker Compose images
  community.docker.docker_compose_v2_pull:
    api_version: '{{ item.api_version | default(omit) }}'
    ca_path: '{{ item.ca_path | default(omit) }}'
    check_files_existing: '{{ item.check_files_existing | default(omit) }}'
    cli_context: '{{ item.cli_context | default(omit) }}'
    client_cert: '{{ item.client_cert | default(omit) }}'
    client_key: '{{ item.client_key | default(omit) }}'
    definition: '{{ item.definition | default(omit) }}'
    docker_cli: '{{ item.docker_cli | default(omit) }}'
    docker_host: '{{ item.docker_host | default(omit) }}'
    env_files: '{{ item.env_files | default(omit) }}'
    files: '{{ item.files | default(omit) }}'
    profiles: '{{ item.profiles | default(omit) }}'
    project_name: '{{ item.project_name | default(omit) }}'
    project_src: '{{ item.project_src | default(omit) }}'
    services: '{{ item.services | default(omit) }}'
    tls: '{{ item.tls | default(omit) }}'
    tls_hostname: '{{ item.tls_hostname | default(omit) }}'
    validate_certs: '{{ item.validate_certs | default(omit) }}'
  become: true
  with_items: '{{ docker_composes | default([]) }}'
  no_log: '{{ not (no_log_disable | default(false)) }}'
- name: Check for Docker Compose changes
  community.docker.docker_compose_v2:
    api_version: '{{ item.api_version | default(omit) }}'
    build: '{{ item.build | default(omit) }}'
    ca_path: '{{ item.ca_path | default(omit) }}'
    check_files_existing: '{{ item.check_files_existing | default(omit) }}'
    cli_context: '{{ item.cli_context | default(omit) }}'
    client_cert: '{{ item.client_cert | default(omit) }}'
    client_key: '{{ item.client_key | default(omit) }}'
    definition: '{{ item.definition | default(omit) }}'
    dependencies: '{{ item.dependencies | default(omit) }}'
    docker_cli: '{{ item.docker_cli | default(omit) }}'
    docker_host: '{{ item.docker_host | default(omit) }}'
    env_files: '{{ item.env_files | default(omit) }}'
    files: '{{ item.files | default(omit) }}'
    profiles: '{{ item.profiles | default(omit) }}'
    project_name: '{{ item.project_name | default(omit) }}'
    project_src: '{{ item.project_src | default(omit) }}'
    pull: 'never'
    recreate: '{{ item.recreate | default(omit) }}'
    remove_images: '{{ item.remove_images | default(omit) }}'
    remove_orphans: '{{ item.remove_orphans | default(omit) }}'
    remove_volumes: '{{ item.remove_volumes | default(omit) }}'
    scale: '{{ item.scale | default(omit) }}'
    services: '{{ item.services | default(omit) }}'
    state: '{{ item.state | default(omit) }}'
    timeout: '{{ item.timeout | default(omit) }}'
    tls: '{{ item.tls | default(omit) }}'
    tls_hostname: '{{ item.tls_hostname | default(omit) }}'
    validate_certs: '{{ item.validate_certs | default(omit) }}'
    wait: '{{ item.wait | default(omit) }}'
    wait_timeout: '{{ item.wait_timeout | default(omit) }}'
  check_mode: true
  become: true
  with_items: '{{ docker_composes | default([]) }}'
  register: res_compose_recreate
  no_log: '{{ not (no_log_disable | default(false)) }}'
- name: Start Docker Compose application
  community.docker.docker_compose_v2:
    api_version: '{{ item.api_version | default(omit) }}'
    build: '{{ item.build | default(omit) }}'
    ca_path: '{{ item.ca_path | default(omit) }}'
    check_files_existing: '{{ item.check_files_existing | default(omit) }}'
    cli_context: '{{ item.cli_context | default(omit) }}'
    client_cert: '{{ item.client_cert | default(omit) }}'
    client_key: '{{ item.client_key | default(omit) }}'
    definition: '{{ item.definition | default(omit) }}'
    dependencies: '{{ item.dependencies | default(omit) }}'
    docker_cli: '{{ item.docker_cli | default(omit) }}'
    docker_host: '{{ item.docker_host | default(omit) }}'
    env_files: '{{ item.env_files | default(omit) }}'
    files: '{{ item.files | default(omit) }}'
    profiles: '{{ item.profiles | default(omit) }}'
    project_name: '{{ item.project_name | default(omit) }}'
    project_src: '{{ item.project_src | default(omit) }}'
    pull: '{{ item.pull | default(omit) }}'
    recreate: '{{ item.recreate | default(omit) }}'
    remove_images: '{{ item.remove_images | default(omit) }}'
    remove_orphans: '{{ item.remove_orphans | default(omit) }}'
    remove_volumes: '{{ item.remove_volumes | default(omit) }}'
    scale: '{{ item.scale | default(omit) }}'
    services: '{{ item.services | default(omit) }}'
    state: '{{ item.state | default(omit) }}'
    timeout: '{{ item.timeout | default(omit) }}'
    tls: '{{ item.tls | default(omit) }}'
    tls_hostname: '{{ item.tls_hostname | default(omit) }}'
    validate_certs: '{{ item.validate_certs | default(omit) }}'
    wait: '{{ item.wait | default(omit) }}'
    wait_timeout: '{{ item.wait_timeout | default(omit) }}'
  become: true
  with_items: >-
    {{
    docker_composes | default([]) |
    selectattr("project_name", "in",
      res_compose_recreate.results |
      selectattr("changed") |
      map(attribute="item.project_name") |
      list
    ) |
    map("combine", {"state": "absent"}) +
    docker_composes | default([])
    }}
  no_log: '{{ not (no_log_disable | default(false)) }}'
